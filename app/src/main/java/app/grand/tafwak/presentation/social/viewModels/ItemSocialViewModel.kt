package app.grand.tafwak.presentation.social.viewModels

import app.grand.tafwak.domain.settings.models.SettingsData
import app.grand.tafwak.presentation.base.BaseViewModel

class ItemSocialViewModel  constructor(val settingsData: SettingsData) : BaseViewModel()