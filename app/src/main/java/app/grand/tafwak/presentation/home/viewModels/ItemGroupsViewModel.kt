package app.grand.tafwak.presentation.home.viewModels

import app.grand.tafwak.domain.home.models.Classes
import app.grand.tafwak.presentation.base.BaseViewModel

class ItemGroupsViewModel  constructor(val classes: Classes) : BaseViewModel()