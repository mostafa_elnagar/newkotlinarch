package app.grand.tafwak.presentation.home.viewModels

import app.grand.tafwak.domain.home.models.Instructor
import app.grand.tafwak.presentation.base.BaseViewModel

class ItemTeacherViewModel  constructor(val instructor: Instructor) : BaseViewModel()