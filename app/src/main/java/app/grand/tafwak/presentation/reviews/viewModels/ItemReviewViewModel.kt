package app.grand.tafwak.presentation.reviews.viewModels

import app.grand.tafwak.domain.reviews.entity.Reviews
import app.grand.tafwak.presentation.base.BaseViewModel

class ItemReviewViewModel  constructor(val reviews: Reviews) : BaseViewModel()