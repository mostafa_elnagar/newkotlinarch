package app.grand.tafwak.presentation.group_details.viewModels

import app.grand.tafwak.domain.groups.entity.Scheduled
import app.grand.tafwak.presentation.base.BaseViewModel

class ItemSessionScheduledViewModel constructor(val scheduled: Scheduled) : BaseViewModel()