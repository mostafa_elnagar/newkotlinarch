package app.grand.tafwak.presentation.teachers.viewModels

import app.grand.tafwak.domain.home.models.Subject
import app.grand.tafwak.presentation.base.BaseViewModel

class ItemSubjectsViewModel  constructor(val subject: Subject) : BaseViewModel()