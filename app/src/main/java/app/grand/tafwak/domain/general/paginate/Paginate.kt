package app.grand.tafwak.domain.general.paginate

open class Paginate(
  val meta: Meta = Meta(),
  val links: Links = Links(),
) {
}